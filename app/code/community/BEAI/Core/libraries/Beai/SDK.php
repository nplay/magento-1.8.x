<?php

namespace Beai;
use Beai\Endpoint\Token;
use Beai\Endpoint\V1;
use Beai\Model\Enums;

class SDK
{
   public $enums = null;

   public function __construct($publicKey = null)
   {
      if($publicKey)
         $this->setPublicKey($publicKey);

      $this->enums = new Enums();
      $this->v1 = new V1();
   }

   public function setPublicKey($publicKey)
   {
      Token::setPublicKey($publicKey);
      return $this;
   }
}