<?php

namespace Beai\Endpoint\V1\Item;

use Beai\Model\ItemService\Body;
use Beai\Helper\RequestAPIHelper;
use Beai\Endpoint\Token;

class Publish
{
    public function __construct()
    {
       $this->endpoint = 'v1/item/publish';
    }

    public function post(Body $body)
    {
        $helper = new RequestAPIHelper($this->endpoint, 'POST', Token::getPublicKey());
        $helper->setBody($body);
        
        return $helper->request();
    }
}