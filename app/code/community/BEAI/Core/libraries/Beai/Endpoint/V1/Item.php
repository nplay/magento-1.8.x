<?php

namespace Beai\Endpoint\V1;

use Beai\Endpoint\V1\Item\Publish;

class Item
{
    public function __construct()
    {
       $this->endpoint = 'item';
       $this->publish = new Publish();
    }
}
