<?php

namespace Beai\Endpoint;

class Token
{
   static $publicKey;

   /**
    * @param String
    */
   public static function setPublicKey($publicKey)
   {
      self::$publicKey = $publicKey;
   }

   public static function getPublicKey()
   {
      return self::$publicKey;
   }
}
