<?php

namespace Beai\Endpoint;

use Beai\Endpoint\V1\Item;

class V1
{
    public function __construct()
    {
       $this->endpoint = 'v1';

       $this->item = new Item();
    }
}
