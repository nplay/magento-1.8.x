<?php
namespace Beai\Helper;

use Beai\Helper\RequestHelper;

class RequestAPIHelper extends RequestHelper
{
   public function __construct($endpoint, $method, $token = null)
   {
      $this->_url = "https://item.service.nplay.com.br/{$endpoint}";
      $this->_method = $method;

      if($token)
         $this->addHeader(['Authorization-token' => $token]);
   }

   public function setBody($param)
   {
      return $this->setPostField($param);
   }

   public function addHeader($params)
   {
      return parent::addHeader($params);
   }

   public function addParameter($params)
   {
      return parent::addParameter($params);
   }

   public function request()
   {
      $response = $this->curl();
      return json_decode($response, true);
   }
}
