<?php

namespace Beai\Helper;

class RequestHelper
{
   protected $_url = null;
   protected $_header = ["Content-Type: application/json"];
   protected $_timeout = 5;
   protected $_curlOptions = null;
   protected $_method = 'GET';
   protected $_postFields = array();
   protected $_parameters = array();

   protected function chargeCurlOptions()
   {
      return $this->_curlOptions = array(
         //CURLOPT_PORT => $this->_port,
         CURLOPT_URL => "{$this->_url}{$this->buildParameters()}",
         CURLOPT_RETURNTRANSFER => true,
         CURLOPT_TIMEOUT => $this->_timeout,
         CURLOPT_FRESH_CONNECT => 1,
         CURLOPT_FORBID_REUSE => 1,
         //CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
         CURLOPT_CUSTOMREQUEST => $this->_method,
         CURLOPT_POSTFIELDS => json_encode($this->_postFields, true),
         CURLOPT_HTTPHEADER => $this->_header,
         CURLOPT_SSL_VERIFYHOST => 0
      );
   }

   private function buildParameters()
   {
      return sizeof($this->_parameters) == 0 ? '' : '?' . http_build_query($this->_parameters);
   }

   protected function addHeader($params)
   {
      foreach($params as $key => $value)
         $this->_header = array_merge($this->_header, ["{$key}: $value"]);
   }

   protected function addParameter($params)
   {
      $this->_parameters = array_merge($this->_parameters, $params);
   }

   protected function addPostField($params)
   {
      $this->_postFields = array_merge($this->_postFields, $params);
   }

   protected function setPostField($object)
   {
      $this->_postFields = $object;
   }

   protected function curl()
   {
      $options = $this->chargeCurlOptions();
      $curl = curl_init();
      curl_setopt_array($curl, $options);

      $response = curl_exec($curl);
      $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
      $err = curl_error($curl);

      //var_dump($options, $response, $err);die;

      if($err || $httpcode != 200)
      {
         $err = $err ? $err : "status code is: {$httpcode}";
         throw new \Exception($err);
      }

      return $response;
   }
}
