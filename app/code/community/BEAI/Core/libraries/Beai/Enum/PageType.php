<?php

namespace Beai\Enum;

class PageType
{
    const HOME = 'home';
    const RESULT = 'result';
    const CATEGORY = 'category';
    const PRODUCT = 'product';
    const CART = 'cart';
    const OTHER = 'other';
    const PURCHASE = 'checkout';
    const TRANSACTION = 'transaction';
}