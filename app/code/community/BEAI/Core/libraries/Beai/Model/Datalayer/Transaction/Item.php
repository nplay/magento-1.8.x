<?php

namespace Beai\Model\Datalayer\Transaction;

class Item
{
    public $id = null;
    public $entityId = null;
    public $name = null;
    public $quantity = null;
    public $price = null;
}