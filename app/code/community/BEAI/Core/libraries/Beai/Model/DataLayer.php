<?php

namespace Beai\Model;
use Beai\Model\Datalayer\Transaction;

class DataLayer
{
    public $pageType = null;

    public $pageCategoryID = null;

    /**
     * @param Beai\Model\Datalayer\User
     */
    public $user = null;

    /**
     * @param Beai\Model\Datalayer\Product
     */
    public $products = [];

    /**
     * @param Beai\Model\Datalayer\Transaction
     */
    public $transaction = null;
}