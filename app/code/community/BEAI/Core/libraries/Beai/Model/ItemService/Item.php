<?php

namespace Beai\Model\ItemService;

class Item
{
    /**
     * SKU
     */
    public $id = null;

    public function __set($name, $value)
    {
        $name = str_replace('set');

        $this[$name] = $value;
        return $this;
    }
}