<?php

namespace Beai\Model\Datalayer;
use Beai\Model\Datalayer\Transaction\Item;

class Transaction
{
    public $id = null;

    public $total = null;

    public $tax = null;

    /**
     * @param Beai\Model\Datalayer\Transaction\Item
     */
    public $itens = [];
}