<?php

namespace Beai\Model;

use Beai\Enum\PageType;

class Enums
{
    public $pageType = null;

    public function __construct()
    {
        $this->pageType = new PageType;
    }
}