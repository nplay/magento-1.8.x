<?php

namespace Beai\Model\ItemService;

use Beai\Model\ItemService\Item;

class Param
{
    const DELETE = 'delete';
    const UPDATE = 'update';
    const INSERT = 'insert';

    /**
     * @param {Beai\Model\ItemService\Item}
     */
    public $item = null;

    public $action = null;

    public function __construct()
    {
        $this->item = new Item();
    }
}