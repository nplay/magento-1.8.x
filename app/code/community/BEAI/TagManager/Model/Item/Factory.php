<?php

class BEAI_TagManager_Model_Item_Factory extends Mage_Core_Model_Abstract
{
	protected $_product = null;

	public function setProduct($product)
	{
		$this->_product = $product;
		return $this;
	}

	public function parse($entityID = null, $storeID = null)
	{
		if($this->_product == null)
		{
			$model = Mage::getModel('catalog/product');
			$model = $storeID ? $model->setStoreId($storeID) : $model;

			$this->setProduct($model->load($entityID));
		}
		
		$product = $this->_product;
		
		$this->item = new \StdClass;
		$this->item->entityId = $product->getId();
		$this->item->id = $product->getSku();
		$this->item->title = $product->getName();
		$this->item->description = substr(Mage::helper('core')->escapeHtml(strip_tags($product->getDescription())), 0, 5000);
		$this->item->image_link = $this->imageLink($product);
		$this->item->product_type = null;
		$this->item->link = '/'.$product->getUrlPath();
		$this->item->allowed = $this->isAllowed($product);
		$this->item->price = $this->price($product);
		$this->item->categorys = $this->categoryIds($product);
		$this->specialPrice($product);
		$this->item->created_at = date('c', strtotime($product->getCreatedAt()));
		$this->item->hasNew = $this->hasNew($product);

		return $this->item;
	}

	protected function hasNew($product)
	{
		$curDate = Mage::getModel('core/date')->date('Y-m-d');
		$curDateStamp = strtotime($curDate);

			$from = $product->getData('news_from_date');
		$to = $product->getData('news_to_date');
		
		if($from == null && $to == null)
		    return false;
		if($from && $to == null)
		    return strtotime($from) <= $curDateStamp;
			
			return $curDateStamp >= strtotime($from) && $curDateStamp <= strtotime($to);
	}

	protected function isAllowed($product)
	{
		$visibility = $product->getVisibility() != Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE;

		if(!$visibility)
			return false;
		
		$isSalable = $product->getTypeInstance(true)->isSalable($product);

		if(!$isSalable)
			return false;

		return $product->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_ENABLED;
	}

	protected function categoryIds($product)
	{
		$ids = $product->getCategoryIds();

		return array_map(function($id){
			return intval($id);
		}, $ids);
	}

	protected function specialPrice($product)
	{
		if($product->getSpecialPrice())
			$this->item->special_price = $product->getSpecialPrice();

		return $this;
	}

	protected function price($product)
	{
        if ($product->getTypeId() === Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
			/** @var Mage_Bundle_Model_Product_Price $bundlePriceModel */
			$bundlePriceModel = Mage::getModel('bundle/product_price');
			return $bundlePriceModel->getTotalPrices($product, 'min', true);
        }

		$price = $product->getSpecialPrice() ? Mage::helper('tax')->getPrice($product, $product->getPrice()) : Mage::helper('tax')->getPrice($product, $product->getFinalPrice());
		return $price;
	}

	protected function imageLink($product)
	{
		$imageLink = null;

		try
		{
			$imageLink = '/'.Mage_Core_Model_Store::URL_TYPE_MEDIA.'/catalog/product'.$product->getImage();
		}
		catch(\Exception $e)
		{
			$imageLink = null;
		}
		finally
		{
			return $imageLink;
		}
	}
}
