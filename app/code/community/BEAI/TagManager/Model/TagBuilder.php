<?php

$helper = Mage::helper('beai_core');
$helper->SDK();

use Beai\Model\DataLayer;
use Beai\Model\Datalayer\Product;
use Beai\Model\Datalayer\Transaction;
use Beai\Model\Datalayer\Transaction\Item;
use Beai\Model\Datalayer\User;
use Beai\Enum\PageType;

class BEAI_TagManager_Model_TagBuilder extends Mage_Core_Model_Abstract
{
    /**
     * Datalayer variable
     */
    protected $_dataLayer = null;

    /**
     * @var string
     */
    protected $fullActionName;

    public function __construct()
    {
        $sdk = Mage::helper('beai_core')->getSDK();

        $this->_pageType = [
            'index' => PageType::HOME,
            'result' => PageType::RESULT,
            'category' => PageType::CATEGORY,
            'product' => PageType::PRODUCT,
            'cart' => PageType::CART,
            'other' => PageType::OTHER,
            'onepage' => PageType::PURCHASE,
            'transaction' => PageType::TRANSACTION
        ];

        $this->fullActionName = Mage::app()->getFrontController()->getAction() ? Mage::app()->getFrontController()->getAction()->getFullActionName() : null;
    }

    protected function categoryID()
    {
        $category = Mage::registry('current_category');
        return !$category ? null : $category->getId();
    }

    protected function pageType()
    {
        $found = preg_match_all('/^.+?_success$/', $this->fullActionName);
        $controllerName = $found != false && $found > 0 ? 'transaction' : Mage::app()->getFrontController()->getRequest()->getControllerName();
        $value = array_key_exists($controllerName, $this->_pageType) ? $this->_pageType[$controllerName] : $this->_pageType['other'];

        return $value;
    }

    protected function buildDatalayer()
    {
        $this->_dataLayer = new DataLayer();
        $this->_dataLayer->pageType = $this->pageType();
        $this->_dataLayer->pageCategoryID = $this->categoryID();
        $this->_dataLayer->fullActionTest = Mage::app()->getFrontController()->getAction()->getFullActionName();

        try {

            $method = sprintf('build%s', $this->_dataLayer->pageType);
        
            if(method_exists($this, $method))
                call_user_func_array([$this, $method], []);

            //All pages tag's
            $this->buildUser();
            
        } catch (\Exception $e) {
            Mage::log($e->getMessage(), null, 'beai_vitrine.log');
        }
    }

    protected function buildproduct()
    {
        $currentProduct = Mage::registry('current_product');

        if(!$currentProduct)
            return false;

        $dataLayerProduct = Mage::getModel('beai_tagmanager/item_factory')
            ->setProduct($currentProduct)
            ->parse();
        
        $this->_dataLayer->products[] = $dataLayerProduct;
    }

    protected function getQuote()
    {
        if(null === $this->_quote)
            $this->_quote = Mage::getSingleton('checkout/cart')->getQuote();
        
        return $this->_quote;
    }

    protected function buildcart()
    {
        $quote = $this->getQuote();
        $storeId = Mage::app()->getStore()->getId();
        
        foreach($quote->getAllVisibleItems() as $quoteItem)
        {
            $item = Mage::getModel('beai_tagmanager/item_factory')
            ->parse($quoteItem->getProduct()->getId(), $storeId);
            
            $this->_dataLayer->products[] = $item;
        }
    }

    protected function buildUser()
    {
        $isLogged = Mage::getSingleton('customer/session')->isLoggedIn();

        if(!$isLogged)
            return $this;

        $user = Mage::getSingleton('customer/session')->getCustomer();
        $this->_dataLayer->user = new User();
        $this->_dataLayer->user->name = $user->getName();
        $this->_dataLayer->user->email = $user->getEmail();
        $this->_dataLayer->user->sId = $user->getId();
    }

    protected function buildtransaction()
    {
        $incrementID = Mage::getSingleton('checkout/session')->getLastRealOrderId();
        $order = Mage::getModel('sales/order')->loadByIncrementId($incrementID);

        $this->_dataLayer->transaction = new Transaction();

        $totalItens = 0;

        foreach($order->getAllVisibleItems() as $idx => $quoteItem)
        {
            $item = new Item();

            $item->id = $quoteItem->getSku();
            $item->entityId = $quoteItem->getProductId();
            $item->name = $quoteItem->getName();
            $item->price = $quoteItem->getPrice();
            $item->quantity = $quoteItem->getQtyOrdered();

            $this->_dataLayer->transaction->itens[] = $item;
        }

        $this->_dataLayer->transaction->id = $incrementID;
        $this->_dataLayer->transaction->total = $order->getBaseGrandTotal();
        $this->_dataLayer->transaction->tax = $order->getBaseGrandTotal() - $order->getBaseSubtotal() . "";
    }

    public function JSON()
    {
        $this->buildDatalayer();
        return json_encode($this->_dataLayer);
    }
}