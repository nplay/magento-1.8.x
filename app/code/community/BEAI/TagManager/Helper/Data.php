<?php
class BEAI_TagManager_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function enabled()
    {
       return Mage::helper('core')->isModuleEnabled('BEAI_TagManager')
          && Mage::getStoreConfig('beai_tag_manager/beai_tag_manager_general/active') == 1;
    }
}
	 