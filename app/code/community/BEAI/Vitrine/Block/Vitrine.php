<?php

class BEAI_Vitrine_Block_Vitrine extends Mage_Core_Block_Template
{
    public function scripts()
    {
        $coreHlp = Mage::helper('beai_core');
        $keys = $coreHlp->keys();

        $scripts = array();

        foreach($keys as $key)
            $scripts[] = $this->script($key);

        return join(PHP_EOL.PHP_EOL, $scripts);
    }

    protected function script($token)
    {
        $scriptUrl = Mage::helper('beai_vitrine')->scriptURLLoader($token);
        return sprintf('<script async src="%s"></script>', $scriptUrl);
    }
}