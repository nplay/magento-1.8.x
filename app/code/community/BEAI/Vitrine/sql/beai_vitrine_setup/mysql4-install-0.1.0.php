<?php
$installer = $this;

$installer->startSetup();

/**
 * SETUP TABLE
 */
$table = $installer->getConnection()
    ->newTable('beai_vitrine_trigger')
    ->addColumn(
        'id', Varien_Db_Ddl_Table::TYPE_INTEGER, null,
        array(
            'identity' => true,
            'unsigned' => true,
            'nullable' => false,
            'primary'  => true,
        ), 'Unique identifier'
    )
    ->addColumn(
        'action', Varien_Db_Ddl_Table::TYPE_VARCHAR, 100, array(), 'insert|update|delete'
    )
    ->addColumn(
        'entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(), 'product entity_id'
    )
    ->addColumn('date', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
    ), 'Date event');

    $installer->run("DROP TABLE IF EXISTS {$this->getTable('beai_vitrine_trigger')};");
    $installer->getConnection()->createTable($table);

/**
 * SETUP TRIGGER'S
 */
$resource = Mage::getSingleton('core/resource');
$writeConnection = $resource->getConnection('core_write');

$writeConnection->query("
DROP TRIGGER if exists beai_vitrine_catalog_product_entity_insert_trigger;
CREATE TRIGGER beai_vitrine_catalog_product_entity_insert_trigger AFTER INSERT ON `catalog_product_entity`
FOR EACH ROW BEGIN
DELETE FROM `beai_vitrine_trigger` WHERE  `entity_id`= NEW.entity_id AND `action` = 'insert';
INSERT INTO `beai_vitrine_trigger` (action, entity_id, date) VALUES ('insert', NEW.entity_id, NOW());
END");

$writeConnection->query("
DROP TRIGGER if exists beai_vitrine_catalog_product_entity_update_trigger;
CREATE TRIGGER beai_vitrine_catalog_product_entity_update_trigger AFTER UPDATE ON `catalog_product_entity`
FOR EACH ROW BEGIN
DELETE FROM `beai_vitrine_trigger` WHERE `entity_id` = NEW.entity_id AND `action` IN ('update', 'insert');
INSERT INTO `beai_vitrine_trigger` (action, entity_id, date) VALUES ('update', NEW.entity_id, NOW());
END");

$writeConnection->query("
DROP TRIGGER if exists beai_vitrine_catalog_product_entity_delete_trigger;
CREATE TRIGGER beai_vitrine_catalog_product_entity_delete_trigger AFTER DELETE ON `catalog_product_entity`
FOR EACH ROW BEGIN
DELETE FROM `beai_vitrine_trigger` WHERE `entity_id`= OLD.entity_id;
INSERT INTO `beai_vitrine_trigger` (action, entity_id, date) VALUES ('delete', OLD.entity_id, NOW());
END");

$installer->endSetup();