<?php

class BEAI_Vitrine_Model_Request extends Mage_Core_Model_Abstract
{
    public $tokens = null;
    public $body = null;

    public function __construct()
    {
        $this->sdk = Mage::helper('beai_core')->getSDK();
    }

    public function send()
    {
        if(sizeof($this->body->params) == 0)
            return true;

        foreach($this->tokens as $token)
        {
            $this->sdk->setPublicKey($token);
            $this->sdk->v1->item->publish->post($this->body);
        }

        return true;
    }
}