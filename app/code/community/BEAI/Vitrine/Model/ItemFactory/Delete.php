<?php

class BEAI_Vitrine_Model_ItemFactory_Delete extends BEAI_TagManager_Model_Item_Factory
{
	public function parse($entityID, $storeID)
	{
		$this->item = new \StdClass;
		$this->item->entityId = $entityID;

        return $this->item;
    }
}