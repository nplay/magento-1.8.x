<?php

class BEAI_Vitrine_Model_Product_Api extends Mage_Api_Model_Resource_Abstract
{
    public function removed($products)
    {
        $ids = array_map(function($product){
            return (int)$product->entity_id;
        }, $products);

        $ecomIds = Mage::getModel('catalog/product')
            ->getCollection()
            ->getAllIds();

        $ecomIds = array_flip($ecomIds);
        $notPresentIds = array();

        foreach($ids as $id)
            if(array_key_exists($id, $ecomIds) == false)
                $notPresentIds[] = $id;

        return array_map(function($id){
            return ['entity_id' => $id];
        }, $notPresentIds);
    }
}