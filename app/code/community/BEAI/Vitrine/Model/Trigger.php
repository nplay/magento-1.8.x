<?php

class BEAI_Vitrine_Model_Trigger extends Mage_Core_Model_Abstract
{
    protected $_resourceCollectionName = 'beai_vitrine/trigger_collection';

    protected function _construct()
    {
       parent::_construct();
       $this->_init("beai_vitrine/trigger");
    }
}