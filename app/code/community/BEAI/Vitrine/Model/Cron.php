<?php

class BEAI_Vitrine_Model_Cron
{
	public function checkTrigger()
	{
		if(Mage::helper('beai_vitrine')->enabled() == false)
			return $this;

		try
		{
			$processAllItens = Mage::helper('beai_vitrine')->initialCharge();

			if($processAllItens == true)
			{
				Mage::helper('beai_vitrine/trigger')->populate();
				Mage::helper('beai_vitrine')->initialCharge(false);
			}

			Mage::helper('beai_vitrine/itemService')->send();

			return $this;
		} catch (\Exception $e) {
			Mage::log($e->getMessage() . "\n" .$e->getTraceAsString(), null, 'beai_vitrine.log', true);
		}
	}

	/**
	 * Tests
	 */
	public function teste()
	{
		$this->checkTrigger();
	}
}