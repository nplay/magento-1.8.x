<?php

Mage::helper('beai_core')->getSDK();

use Beai\Model\ItemService\Param;
use Beai\Model\ItemService\Item;

class BEAI_Vitrine_Model_ParamFactory_Delete extends BEAI_Vitrine_Model_ParamFactory_Factory
{
	public function produceParams()
	{
		$params = [];
		
		$this->triggerEvents = $this->triggerHelper->byAction($this->action());

		foreach($this->triggerEvents as $triggerEvent)
		{
			try
			{
				$entityID = $triggerEvent->getEntityID();

				$param = new Param();
				$param->action = $this->action();
				$param->item = $this->produceItem($entityID);
	
				$params[] = $param;
			} catch (\Exception $e) {
				Mage::log("Delete Factory fail produceParams #{$entityID} \nError Message: " . $e->getMessage() . "\nStacktrace: \n" .$e->getTraceAsString(), null, 'beai_vitrine.log', true);
			}
		}

		return $params;
	}

	public function action()
	{
		return BEAI_Vitrine_Helper_Trigger::ACTION_DELETE;
	}

	public function produceItem($entityID)
	{
		return $this->getProduct($entityID);
	}
}