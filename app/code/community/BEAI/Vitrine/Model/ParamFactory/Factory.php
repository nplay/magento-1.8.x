<?php

abstract class BEAI_Vitrine_Model_ParamFactory_Factory
{
	protected $keyObject = null;
	protected $triggerEvents = [];

	/**
	 * Store loaded products
	 */
	protected static $_loadedProducts = [];

	public function __construct()
	{
		$this->triggerHelper = Mage::helper('beai_vitrine/trigger');
	}

	public function action()
	{
		throw new Error('Implement method in child class');
	}

	/**
	 * Get loaded product
	 */
	protected function getProduct($entityID)
	{
		$loadProductKey = sprintf("%s.%s", $this->keyObject->storeID, $entityID);

		if(isset(self::$_loadedProducts[$loadProductKey]))
			return self::$_loadedProducts[$loadProductKey];
		
		return $this->addProduct($entityID);
	}

	/**
	 * Store new product
	 */
	protected function addProduct($entityID)
	{
		$storeID = $this->keyObject->storeID;
		$item = $this->parseProduct($entityID, $storeID);

		$loadProductKey = sprintf("%s.%s", $storeID, $item->id);
		self::$_loadedProducts[$loadProductKey] = $item;

		return self::$_loadedProducts[$loadProductKey];
	}

	protected function parseProduct($entityID, $storeID)
	{
		$itemFactory = Mage::getModel("beai_vitrine/ItemFactory_{$this->action()}");
		return $itemFactory->parse($entityID, $storeID);
	}

	/**
	 * Set object of tokens of storeID
	 */
	public function setKeyObject($keyObject)
	{
		$this->keyObject = $keyObject;
		return $this;
	}

	/**
	 * After send to API, clear trigger registry
	 */
	public function clearTriggerEvents()
	{
		foreach($this->triggerEvents as $triggerEvent)
			$triggerEvent->delete();
	}

	/**
	 * Build parameters of body to API
	 */
	public abstract function produceParams();

	/**
	 * Build item of parameters to request body
	 */
	public abstract function produceItem($entityID);
}