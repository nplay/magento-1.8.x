<?php

class BEAI_Vitrine_Model_Resource_Trigger extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('beai_vitrine/trigger', 'id');
    }
}