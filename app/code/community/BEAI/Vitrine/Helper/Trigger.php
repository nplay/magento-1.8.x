<?php

Mage::helper('beai_core')->getSDK();

use Beai\Model\ItemService\Param;

class BEAI_Vitrine_Helper_Trigger extends Mage_Core_Helper_Abstract
{
    protected $pageSize = 200;

    const ACTION_DELETE = Param::DELETE;
    const ACTION_UPDATE = Param::UPDATE;
    const ACTION_INSERT = Param::INSERT;

    public function setPageSize($size)
    {
        $this->pageSize = $size;
    }

    /**
     * Each all itens in Magento to add in trigger table
     */
    public function populate()
    {
        $triggerItens = $this->entityIds();
        $triggerItens = sizeof($triggerItens) == 0 ? [0] : $triggerItens; //Bug Magento nin array is zero

        $lastPageNumber = Mage::getModel('catalog/product')->getCollection()
            ->addFieldToFilter('entity_id', array('nin'=> $triggerItens))
            ->setPageSize($this->pageSize)
            ->getLastPageNumber();

        $page = 1;
        
        do
        {
            $collection = Mage::getModel('catalog/product')
                ->getCollection()
                ->addIdFilter($triggerItens, true)
                ->setPageSize($this->pageSize)
                ->setCurPage($page);

            $products = $collection->load();
            $date = Mage::getModel('core/date')->gmtDate('Y-m-d H:i:s');

            foreach($products as $product)
            {
                $triggerModel = Mage::getModel('beai_vitrine/trigger');

                $triggerModel->action = self::ACTION_INSERT;
                $triggerModel->setEntityId($product->getId());
                $triggerModel->setDate($date);
                $triggerModel->save();
            }

        } while(++$page <= $lastPageNumber);

        return true;
    }

    public function entityIds()
    {
        $collection = Mage::getModel('beai_vitrine/trigger')
            ->getCollection()
            ->addFieldToSelect('entity_id');

        return array_map(function($item){
                return $item->getEntityId();
            }, $collection->load()->getItems());
    }

    public function byAction($action)
    {
        return Mage::getModel('beai_vitrine/trigger')
            ->getCollection()
            ->addFieldToFilter('action', $action)
            ->setCurPage(1)
            ->setPageSize($this->pageSize)
            ->load();
    }
}
	 