<?php

class BEAI_Vitrine_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $coreHlp = null;

    public function __construct()
    {
        $this->coreHlp = Mage::helper('beai_core');
    }

    public function enabled()
    {
        $coreEnabled = Mage::helper('beai_core')->enabled();
        $tagManagerEnabled = Mage::helper('beai_tagmanager')->enabled();

        return $coreEnabled && $tagManagerEnabled && Mage::getStoreConfig('beai_vitrine/beai_vitrine_general/active') == 1;
    }

    public function isPreviewMode()
    {
        return Mage::getStoreConfig('beai_vitrine/beai_vitrine_general/preview_mode') == 1;
    }

    public function initialCharge($value = null)
    {
        $namespace = 'beai_vitrine/beai_vitrine_general/initial_charge';

        if($value !== null)
            Mage::getConfig()->saveConfig($namespace, (int)$value);

        $store = Mage::app()->getStore();
        $store->cleanModelCache();

        return $store->getConfig($namespace) == 1;
    }

    public function scriptURLLoader($token)
    {
        return sprintf("%s?token=%s", $this->coreHlp->urlLoader(), $token);
    }
}
	 