<?php

Mage::helper('beai_core')->getSDK();

use Beai\Model\ItemService\Body;

class BEAI_Vitrine_Helper_ItemService extends Mage_Core_Helper_Abstract
{
    protected $keys = [];

    public function __construct()
    {
        $this->keys = Mage::helper('beai_core')->allKeys();
        $this->factorys = [];
    }

    public function send()
    {
        $requests = [];

        foreach($this->keys as $keysObject)
        {
            $request = $this->prepareRequest($keysObject);
            $request->send();

            foreach($this->factorys as $factory)
                $factory->clearTriggerEvents();
        }
    }

    protected function prepareRequest($keysObject)
    {
        $request = Mage::getModel('beai_vitrine/request');
        $request->body = $this->makeBody($keysObject);
        $request->tokens = $keysObject->keys;

        return $request;
    }

    protected function factory($name)
    {
        if(array_key_exists($this->factorys, $name) == false)
            $this->factorys[$name] = Mage::getModel("beai_vitrine/paramFactory_{$name}");

        return $this->factorys[$name];
    }

    protected function makeBody($keysObject)
    {
        $body = new Body();

        $factorysName = ['insert', 'update', 'delete'];

        foreach($factorysName as $factoryName)
        {
            $factory = $this->factory($factoryName)->setKeyObject($keysObject);
            $body->params = array_merge($body->params, $factory->produceParams());
        }

        return $body;
    }
}