<?php

class BEAI_Category_IndexController extends Mage_Core_Controller_Front_Action
{
    public function treeAction()
    {
        $categoriesArray = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSort('path', 'asc')
            ->load();
        
        $returnData = new \StdClass;
        $returnData->data = [];
        $returnData->success = true;
        $returnData->message = null;

        try {
            foreach($categoriesArray as $categorie)
            {
                $returnData->data[] = [
                    "id" => $categorie->getEntityId(),
                    "parent_id" => $categorie->getParentId(),
                    "name" => $categorie->getName(),
                    "path" => $categorie->getPath(),
                    "position" => $categorie->getPosition()
                ];
            }

        } catch (\Exception $e) {
            $returnData->success = false;
            $returnData->message = $e->getMessage();
        }

        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json', true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($returnData));
    }

    public function moveAction()
    {
        $returnData = new \StdClass;
        $returnData->data = 'ok';
        $returnData->success = true;
        $returnData->message = null;

        try
        {
            $categorys = json_decode($this->getRequest()->getRawBody());
	    
            foreach($categorys as $category)
            {    
                if($category->categoryId == null || $category->parentId === null)
                    throw new Exception('Post data invalid');
                
                if($category->categoryId == 1)
                    continue;

                Mage::getModel('catalog/category')->load($category->categoryId)
                ->move($category->parentId, $category->afterId);
            }

        } catch (\Exception $e) {
	    Mage::log($e->getMessage(), null, 'beai_core.log', true);

            $returnData->success = false;
            $returnData->message = $e->getMessage();
        }

        $this->getResponse()->clearHeaders()->setHeader('Content-type','application/json', true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($returnData));
    }
}
